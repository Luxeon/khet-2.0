from algorithms import *

import pygame, sys, copy
from pygame.locals import *

WIDTH = 1280
HEIGHT = 720
BROWN = (147, 92, 51)
WHITE = (255, 255, 255)


def drawButton(maSurface, monTexte, pos_x, pos_y, end_x, end_y):

	myRect = pygame.Rect(pos_x, pos_y, end_x, end_y)

	button = pygame.image.load('assets/sprites/button.png')
	button = pygame.transform.smoothscale(button, (end_x, end_y))
	maSurface.blit(button, (pos_x, pos_y))

	font = pygame.font.Font("assets/LHF_Uncial_Caps.ttf", (end_y * 2)//4)

	content = font.render(monTexte, True, (242, 230, 218))
	contentRect = content.get_rect()
	contentRect.center = (pos_x + end_x / 2, pos_y + end_y / 2)

	maSurface.blit(content, contentRect)

	return myRect


def possible_square(i,j,prv_i,prv_j):
	square = [prv_i - 1, prv_i + 1, prv_j -1, prv_j + 1]

	if prv_i == 0:
		square[0] = prv_i
	elif prv_i == 9:
		square[1] = prv_i
	if prv_j == 0:
		square[2] = prv_j
	elif prv_j == 7:
		square[3] = prv_j

	return square


def step_click_possible(game_board, player, i, j, prv_i, prv_j, step_in_progress):

	if not step_in_progress:
		if not possible(game_board, player, j, i):
			return False
		else:
			return True

	if step_in_progress:
		motion = -1

		square = possible_square(i, j, prv_i, prv_j)

		if game_board[prv_j][prv_i][1] == 1:
			if prv_i - 1 == i or prv_i + 1 == i or prv_j - 1 == j or prv_j + 1 == j:
				motion = 3  # Movement
			else:
				return False

		elif game_board[prv_j][prv_i][1] == 2:
			if prv_i - 1 <= i <= prv_i + 1 and prv_j - 1 <= j <= prv_j + 1:
				if prv_i == i and prv_j == j:
					motion = 1  # Rotation
				else:
					motion = 3  # Movement

			else:
				return False

		elif game_board[prv_j][prv_i][1] == 3 or game_board[prv_j][prv_i][1] == 4:
			if prv_i == 0 and prv_j == j:
				if i == -1:
					motion = 2  # Counterclockwise rotation
				elif i == 2:
					motion = 1  # Clockwise rotation
				elif i == 1:
					motion = 3  # Movement

			elif prv_i == 9 and prv_j == j:
				if i == 7:
					motion = 2
				elif i == 10:
					motion = 1
				elif i == 8:
					motion = 3

			elif prv_i - 2 == i and prv_j == j:
				motion = 2
			elif prv_i + 2 == i and prv_j == j:
				motion = 1
			elif prv_i - 1 == i or prv_i + 1 == i or prv_j - 1 == j or prv_j + 1 == j:
				motion = 3
			else:
				return False

		elif game_board[prv_j][prv_i][1] == 5:
			if prv_i == i and prv_j == j:
				motion = 1
			else:
				return False

		if motion == 3:  # Movement
			if square[0] <= i <= square[1] and square[2] <= j <= square[3]:
				if not ((player == 1 and (i == 9 or (i == 1 and (j == 0 or j == 7)))) or
							(player == 2 and (i == 0 or (i == 8 and (j == 0 or j == 7))))):
					if game_board[j][i][0] != 0 and (prv_i != i or prv_j != j):  # Occupied slot
						if game_board[prv_j][prv_i][1] == 2 and (game_board[j][i][1] == 3 or game_board[j][i][1] == 4):
							return True
						else:
							return False
					else:
						return True
				else:
					return False
			else:
				return False

		elif motion == -1:
			return False

		else:
			return True


def actions_click(game_board, i, j, prv_i, prv_j):

	if game_board[prv_j][prv_i][1] == 1:
		return 3

	elif game_board[prv_j][prv_i][1] == 2:
		if prv_i == i and prv_j == j:
			return 1

	elif game_board[prv_j][prv_i][1] == 5:
		return 1

	elif prv_j == j:
		if prv_i == 0:
			if i == 2:
				return 1
			elif i == -1:
				return 2

		elif prv_i == 9:
			if i == 10:
				return 1
			elif i == 7:
				return 2

		elif 0 < prv_i < 9:
			if i == prv_i + 2:
				return 1
			elif i == prv_i - 2:
				return 2

	if game_board[j][i][0] == 0 or \
		(game_board[prv_j][prv_i][1] == 2 and (game_board[j][i][1] == 3 or game_board[j][i][1] == 4)):
		# Move pawn
		return 3


def move_pawn(game_board, i, j, prv_i, prv_j, player):
	if not (0 <= i <= 9 and 0 <= j <= 7):
		return False
	elif (player == 1 and (i == 9 or (i == 1 and (j == 0 or j == 7)))) or (
					player == 2 and (i == 0 or (i == 8 and (j == 0 or j == 7)))):
		return False
	elif game_board[j][i][0] != 0:
		if game_board[prv_j][prv_i][1] == 2 and (game_board[j][i][1] == 3 or game_board[j][i][1] == 4):
			tmp = game_board[prv_j][prv_i]
			game_board[prv_j][prv_i] = game_board[j][i]
			game_board[j][i] = tmp
			return True
		else:
			return False
	else:
		game_board[j][i] = game_board[prv_j][prv_i]
		game_board[prv_j][prv_i] = [0, 0, 0]
		return True


def rotate_pawn(game_board, i, j, is_clockwise):

	if game_board[j][i][1] == 5 or game_board[j][i][1] == 2:
		if game_board[j][i][2] == 1:
			game_board[j][i][2] = 4
		elif game_board[j][i][2] == 2:
			game_board[j][i][2] = 3
		elif game_board[j][i][2] == 3:
			game_board[j][i][2] = 2
		elif game_board[j][i][2] == 4:
			game_board[j][i][2] = 1

	else:
		if is_clockwise:
			if game_board[j][i][2] != 4:
				game_board[j][i][2] += 1
			else:
				game_board[j][i][2] = 1

		else:  # counterclockwise
			if game_board[j][i][2] != 1:
				game_board[j][i][2] -= 1
			else:
				game_board[j][i][2] = 4


def select_cursor(pos_x, pos_y, cell_size):
	i = -2
	j = -2

	for line in range(-1, 11):
		for col in range(8):
			x_axis = 105 + line * cell_size
			y_axis = 93 + col * cell_size

			if x_axis < pos_x < x_axis + cell_size and y_axis < pos_y < y_axis + cell_size:
				i = line
				j = col

	if -1 <= i <= 10 and 0 <= j <= 7:
		return i, j
	else:
		return -2, -2


def get_player_color(player):
	if player == 1:
		# Color is RED
		player_color = (226, 55, 51)
	else:
		# Color is WHITE
		player_color = (255, 255, 255)

	return player_color


def draw_cell(my_surface, game_board, line, col, cell_size):
	# Color is RED
	if game_board[col][line][0] == 1:
		if game_board[col][line][1] == 1:
			pawn = pygame.image.load("assets/sprites/" + str(game_board[col][line][2]) + "/pharaon_red.png")
		elif game_board[col][line][1] == 2:
			pawn = pygame.image.load("assets/sprites/" + str(game_board[col][line][2]) + "/scarab_red.png")
		elif game_board[col][line][1] == 3:
			pawn = pygame.image.load("assets/sprites/" + str(game_board[col][line][2]) + "/anubis_red.png")
		elif game_board[col][line][1] == 4:
			pawn = pygame.image.load("assets/sprites/" + str(game_board[col][line][2]) + "/pyramid_red.png")
		elif game_board[col][line][1] == 5:
			pawn = pygame.image.load("assets/sprites/" + str(game_board[col][line][2]) + "/sphynx_red.png")

	# Color is WHITE
	elif game_board[col][line][0] == 2:
		if game_board[col][line][1] == 1:
			pawn = pygame.image.load("assets/sprites/" + str(game_board[col][line][2]) + "/pharaon_white.png")
		elif game_board[col][line][1] == 2:
			pawn = pygame.image.load("assets/sprites/" + str(game_board[col][line][2]) + "/scarab_white.png")
		elif game_board[col][line][1] == 3:
			pawn = pygame.image.load("assets/sprites/" + str(game_board[col][line][2]) + "/anubis_white.png")
		elif game_board[col][line][1] == 4:
			pawn = pygame.image.load("assets/sprites/" + str(game_board[col][line][2]) + "/pyramid_white.png")
		elif game_board[col][line][1] == 5:
			pawn = pygame.image.load("assets/sprites/" + str(game_board[col][line][2]) + "/sphynx_white.png")

	if game_board[col][line][0] != 0:
		pawn = pygame.transform.smoothscale(pawn, (cell_size, int(cell_size * 550 / 325)))
		pawnRect = pygame.Rect(line * cell_size + 105, col * cell_size + 80 - (int(cell_size * 550 / 325) - cell_size), cell_size, int(cell_size * 550 / 325))
		my_surface.blit(pawn, pawnRect)


def draw_grid(my_surface, cell_size):
	for line in range(10):
		for col in range(8):
			draw_case(my_surface, BROWN, line, col, cell_size)


def draw_case(my_surface, case_color, line, col, cell_size):
	window_gap_x = 105
	window_gap_y = 93
	start_x_axis = line * cell_size + window_gap_x
	start_y_axis = col * cell_size + window_gap_y
	end_x_axis = start_x_axis + cell_size
	end_y_axis = start_y_axis + cell_size

	pygame.draw.line(my_surface, case_color, (start_x_axis, start_y_axis), (end_x_axis, start_y_axis), 3)  # First line bar
	pygame.draw.line(my_surface, case_color, (start_x_axis, end_y_axis), (end_x_axis, end_y_axis), 3)  # Second line bar
	pygame.draw.line(my_surface, case_color, (start_x_axis, start_y_axis), (start_x_axis, end_y_axis), 3)  # First col bar
	pygame.draw.line(my_surface, case_color, (end_x_axis, start_y_axis), (end_x_axis, end_y_axis), 3)  # Second col bar


def draw_single_laser(my_surface, player, cell_size, d, i, j, half):

	half_size_x = int(cell_size * 0.5)
	half_size_y = int(cell_size * 0.75)
	middle_gap_x = int(cell_size * 0.5)
	middle_gap_y = int(cell_size * 0.25)
	window_gap_x = 105
	window_gap_y = 93

	if half == 1:
		start_x_axis = i * cell_size + window_gap_x + middle_gap_x
		start_y_axis = j * cell_size + window_gap_y + middle_gap_y
		end_x_axis = start_x_axis + d[0] * half_size_x
		end_y_axis = start_y_axis + d[1] * half_size_y

	else:
		start_x_axis = i * cell_size + window_gap_x + middle_gap_x + d[0] * half_size_x
		start_y_axis = j * cell_size + window_gap_y + middle_gap_y + d[1] * half_size_y
		end_x_axis = (i + d[0]) * cell_size + window_gap_x + middle_gap_x
		end_y_axis = (j + d[1]) * cell_size + window_gap_y + middle_gap_y

	if player == 1:
		second_color = (190, 51, 49)
	else:
		second_color = (205, 205, 205)

	pygame.draw.line(my_surface, second_color, (start_x_axis, start_y_axis), (end_x_axis, end_y_axis), 5)
	pygame.draw.line(my_surface, get_player_color(player), (start_x_axis, start_y_axis), (end_x_axis, end_y_axis), 3)


def next_lasers_pos(i, j, d):
	if not 0 <= i + d[0] <= 9:
		next_i = i
	else:
		next_i = i + d[0]

	if not 0 <= j + d[1] <= 7:
		next_j = j
	else:
		next_j = j + d[1]

	return next_i, next_j


def draw_laser(my_surface, cell_size, game_board, player, col, scarab_list, do_hide):

	if player == 1:
		i = 0
		j = 0
	else:  # player == 2
		i = 9
		j = 7

	if game_board[j][i][2] == 1:
		d = [0, -1]
	elif game_board[j][i][2] == 2:
		d = [1, 0]
	elif game_board[j][i][2] == 3:
		d = [0, 1]
	else:  # game_board[j][i][2] == 4
		d = [-1, 0]

	end = False

	prv_i = i
	prv_j = j

	prv_d = copy.deepcopy(d)

	while not end:
		if j == col:
			if not do_hide:
				draw_single_laser(my_surface, player, cell_size, d, i, j, 1)
				draw_single_laser(my_surface, player, cell_size, d, i, j, 2)

			else:
				if game_board[j][i][1] == 2 and not is_scarab_front(game_board, i, j, prv_d):
					draw_single_laser(my_surface, player, cell_size, d, i, j, 1)
					draw_single_laser(my_surface, player, cell_size, prv_d, prv_i, prv_j, 2)

		prv_i = i
		prv_j = j

		i += d[0]
		j += d[1]

		if i < 0 or i > 9 or j < 0 or j > 7:
			break

		while game_board[j][i][0] == 0:
			if j == col:
				if not do_hide:
					draw_single_laser(my_surface, player, cell_size, d, i, j, 1)
					draw_single_laser(my_surface, player, cell_size, d, i, j, 2)

			prv_i = i
			prv_j = j

			i += d[0]
			j += d[1]

			if i < 0 or i > 9 or j < 0 or j > 7:
				break

		prv_d = copy.deepcopy(d)
		end = find_collision(d, game_board, i, j)


def find_collision(d, game_board, i, j):

	if i < 0 or i > 9 or j < 0 or j > 7:
		return True

	elif game_board[j][i][1] == 1:
		return True

	elif game_board[j][i][1] == 2:
		if game_board[j][i][2] == 1 or game_board[j][i][2] == 3:
			if d[0] == 0:
				d[0] = d[1]
				d[1] = 0
			else:
				d[1] = d[0]
				d[0] = 0
		elif game_board[j][i][2] == 2 or game_board[j][i][2] == 4:
			if d[0] == 0:
				d[0] = - d[1]
				d[1] = 0
			else:
				d[1] = - d[0]
				d[0] = 0

		return False

	elif game_board[j][i][1] == 3:
		return True

	elif game_board[j][i][1] == 4:
		if ((d == [1, 0] or d == [0, -1]) and game_board[j][i][2] == 1) or \
				((d == [1, 0] or d == [0, 1]) and game_board[j][i][2] == 2) or \
				((d == [-1, 0] or d == [0, 1]) and game_board[j][i][2] == 3) or \
				((d == [-1, 0] or d == [0, -1]) and game_board[j][i][2] == 4):
			return True

		else:
			if game_board[j][i][2] == 1 or game_board[j][i][2] == 3:
				if d[0] == 0:
					d[0] = d[1]
					d[1] = 0
				else:
					d[1] = d[0]
					d[0] = 0
			elif game_board[j][i][2] == 2 or game_board[j][i][2] == 4:
				if d[0] == 0:
					d[0] = - d[1]
					d[1] = 0
				else:
					d[1] = - d[0]
					d[0] = 0

			return False

	elif game_board[j][i][1] == 5:
		return True


def is_scarab_front(game_board, i, j, d):

	if game_board[j][i][2] == 1 or game_board[j][i][2] == 3:
		if d[0] == 1 or d[1] == -1:
			# Scarab should be shown before laser
			return False
		if d[0] == -1 or d[1] == 1:
			# Scarab should be shown after laser
			return True

	elif game_board[j][i][2] == 2 or game_board[j][i][2] == 4:
		if d[0] == -1 or d[1] == -1:
			# Scarab should be shown before laser
			return False
		if d[0] == 1 or d[1] == 1:
			# Scarab should be shown after laser
			return True


def get_scarab_list(game_board, player):

	if player == 1:
		i = 0
		j = 0
	else:  # player == 2
		i = 9
		j = 7

	if game_board[j][i][2] == 1:
		d = [0, -1]
	elif game_board[j][i][2] == 2:
		d = [1, 0]
	elif game_board[j][i][2] == 3:
		d = [0, 1]
	else:  # game_board[j][i][2] == 4
		d = [-1, 0]

	end = False
	scarab_list = [[-1] for i in range(2)]
	prv_d = copy.deepcopy(d)

	while not end:
		if game_board[j][i][1] == 2:
			if not is_scarab_front(game_board, i, j, prv_d):
				if scarab_list[0] == [-1]:
					scarab_list[0] = [[i, j]]
				else:
					scarab_list[0].append([i, j])

			else:
				if scarab_list[1] == [-1]:
					scarab_list[1] = [[i, j]]
				else:
					scarab_list[1].append([i, j])

		i += d[0]
		j += d[1]

		if i < 0 or i > 9 or j < 0 or j > 7:
			return scarab_list

		while game_board[j][i][0] == 0:
			i += d[0]
			j += d[1]

			if i < 0 or i > 9 or j < 0 or j > 7:
				return scarab_list

		prv_d = copy.deepcopy(d)
		end = find_collision(d, game_board, i, j)

	return scarab_list


def draw_rotation(my_surface, line, col, cell_size, is_clockwise):

	grid_gap_x = int(85 + cell_size / 2)
	grid_gap_y = int(73 + cell_size / 2)
	x_axis = line * cell_size + grid_gap_x
	y_axis = col * cell_size + grid_gap_y
	radius = int(cell_size * 0.4)

	if is_clockwise:
		right = pygame.image.load("assets/sprites/right_button.png")
		right = pygame.transform.smoothscale(right, (int(cell_size * 2 / 3), int(cell_size * 2 / 3)))
		my_surface.blit(right, (x_axis, y_axis))

	else:  # counterclockwise
		left = pygame.image.load("assets/sprites/left_button.png")
		left = pygame.transform.smoothscale(left, (int(cell_size * 2 / 3), int(cell_size * 2 / 3)))
		my_surface.blit(left, (x_axis, y_axis))


def draw_pawn_choices(my_surface, game_board, player, i, j, cell_size):

	if game_board[j][i][1] == 5:
		if game_board[j][i][2] == 1 or game_board[j][i][2] == 3:
			is_clockwise = False
		else:  # game_board[j][i][2] == 2 or game_board[j][i][2] == 4
			is_clockwise = True

		draw_rotation(my_surface, i, j, cell_size, is_clockwise)

	else:
		for line in range(i - 1, i + 2):
			for col in range(j - 1, j + 2):
				if 0 <= line <= 9 and 0 <= col <= 7:
					if not (line == i and col == j) and step_click_possible(game_board, player, line, col, i, j, True):
						draw_case(my_surface, (36, 189, 158), line, col, cell_size)
						
		for line in range(i - 1, i + 2):
			for col in range(j - 1, 8):
				if 0 <= line <= 9 and 0 <= col <= 7:
					draw_cell(my_surface, game_board, line, col, cell_size)

		if game_board[j][i][1] == 2:
			draw_rotation(my_surface, i, j, cell_size, True)

		elif game_board[j][i][1] != 1:
			if i == 0:
				draw_rotation(my_surface, i + 2, j, cell_size, True)
				draw_rotation(my_surface, i - 1, j, cell_size, False)

			elif i == 9:
				draw_rotation(my_surface, i + 1, j, cell_size, True)
				draw_rotation(my_surface, i - 2, j, cell_size, False)

			else:
				draw_rotation(my_surface, i + 2, j, cell_size, True)
				draw_rotation(my_surface, i - 2, j, cell_size, False)

	pygame.display.update()


def draw_board(my_surface, old_game_board, game_board, player, cell_size, turn, do_show_laser):

	background = pygame.image.load('assets/sprites/plateau_background.png')
	background = pygame.transform.smoothscale(background, (WIDTH, HEIGHT))
	my_surface.blit(background, (0, 0))

	draw_grid(my_surface, cell_size)

	scarab_list = get_scarab_list(old_game_board, player % 2 + 1)

	# step 3 afficher les scarab dans scarab_list[0] et [1] step 4 afficher les derniers scarab seulement dans scarab[1]

	for col in range(8):
		for line in range(10):
			if (game_board[col][line][2] == 2 or game_board[col][line][2] == 3) and not game_board[col][line][1] == 2:
				pass
				draw_cell(my_surface, game_board, line, col, cell_size)

		if turn != 0 and do_show_laser:
			draw_laser(my_surface, cell_size, old_game_board, player % 2 + 1, col, scarab_list, False)

		for line in range(10):
			if game_board[col][line][1] == 2:
				pass
				draw_cell(my_surface, game_board, line, col, cell_size)

		if turn != 0 and do_show_laser:
			draw_laser(my_surface, cell_size, old_game_board, player % 2 + 1, col, scarab_list, True)

		for line in range(10):
			if (game_board[col][line][2] == 1 or game_board[col][line][2] == 4) and not game_board[col][line][1] == 2:
				pass
				draw_cell(my_surface, game_board, line, col, cell_size)

	pygame.display.update()


def main(game_board, n):

	while game_board == -1 or game_board == 1:
		game_board = initGameboard(n)

	old_game_board = copy.deepcopy(game_board)
	turn = 0
	player = 1
	prv_i = -2
	prv_j = -2

	cell_size = 67

	my_surface = pygame.display.set_mode((WIDTH, HEIGHT))

	in_progress = True
	step_in_progress = False

	font = pygame.font.Font("assets/LHF_Uncial_Caps.ttf", 50)

	pygame.display.set_caption("Khet 2.0")
	draw_board(my_surface, game_board, game_board, player, cell_size, turn, False)
	menu = drawButton(my_surface,"Menu",900,500,298,52)
	player_text = font.render("Player " + str(player), True, get_player_color(player))
	player_textRect = player_text.get_rect()
	player_textRect.center = (1055, 200)
	my_surface.blit(player_text, player_textRect)

	pygame.display.update()

	while in_progress:
		for event in pygame.event.get():

			if event.type == MOUSEBUTTONDOWN:

				pos_x, pos_y = event.pos

				i, j = select_cursor(pos_x, pos_y, cell_size)

				if event.button == 1:
					square = possible_square(i, j, prv_i, prv_j)

					if pos_x >= menu.left and pos_x <= menu.right and pos_y >= menu.top and pos_y <= menu.bottom:
						fade = pygame.Surface((WIDTH, HEIGHT))
						fade.set_alpha(200)
						fade.fill((108, 69, 34))
						my_surface.blit(fade, (0, 0))

						leave_text = font.render("Are you sure you want to quit?", True, WHITE)
						leave_textRect = leave_text.get_rect()
						leave_textRect.center = (640, 300)
						my_surface.blit(leave_text, leave_textRect)

						yes = drawButton(my_surface, "Yes", 290, 400, 298, 52)
						no = drawButton(my_surface, "No", 700, 400, 298, 52)

						pygame.display.update()

						in_prog = True

						while in_prog:
							for event in pygame.event.get():
								if event.type == MOUSEBUTTONDOWN:
									x, y = event.pos
									if x >= yes.left and x <= yes.right and y >= yes.top and y <= yes.bottom:
										return 1
									if x >= no.left and x <= no.right and y >= no.top and y <= no.bottom:
										in_prog = False
										draw_board(my_surface, old_game_board, game_board, (turn % 2) + 1, cell_size, turn, True)
										drawButton(my_surface, "Menu", 900, 500, 298, 52)
										player_text = font.render("Player " + str((turn % 2) + 1), True, get_player_color((turn % 2) + 1))
										player_textRect = player_text.get_rect()
										player_textRect.center = (1055, 200)
										my_surface.blit(player_text, player_textRect)

					elif step_click_possible(game_board, player, i, j, prv_i, prv_j, step_in_progress):

						if not step_in_progress:

							step_in_progress = True
							prv_i, prv_j = i, j

							draw_board(my_surface, old_game_board, game_board, (turn % 2) + 1, cell_size, turn, False)
							drawButton(my_surface, "Menu", 900, 500, 298, 52)
							player_text = font.render("Player " + str((turn % 2) + 1), True, get_player_color((turn % 2) + 1))
							player_textRect = player_text.get_rect()
							player_textRect.center = (1055, 200)
							my_surface.blit(player_text, player_textRect)

							draw_pawn_choices(my_surface, game_board, (turn % 2) + 1, i, j, cell_size)

						else:  # step_in_progress

							action = actions_click(game_board, i, j, prv_i, prv_j)

							ok = False
							
							if action == 1:
								rotate_pawn(game_board, prv_i, prv_j, True)
								ok = True
							elif action == 2:
								rotate_pawn(game_board, prv_i, prv_j, False)
								ok = True
							elif action == 3:
								ok = move_pawn(game_board, i, j, prv_i, prv_j, player)

							if ok:
								old_game_board = copy.deepcopy(game_board)

								laser(game_board, player)

								step_in_progress = False

								turn += 1

								draw_board(my_surface, old_game_board, game_board, (turn % 2) + 1, cell_size, turn, True)
								drawButton(my_surface,"Menu",900,500,298,52)
								player_text = font.render("Player " + str((turn % 2) + 1), True, get_player_color((turn % 2) + 1))
								player_textRect = player_text.get_rect()
								player_textRect.center = (1055, 200)
								my_surface.blit(player_text, player_textRect)

					elif step_in_progress:
						step_in_progress = False
						draw_board(my_surface, old_game_board, game_board, (turn % 2) + 1, cell_size, turn, False)
						drawButton(my_surface,"Menu",900,500,298,52)
						player_text = font.render("Player " + str((turn % 2) + 1), True, get_player_color((turn % 2) + 1))
						player_textRect = player_text.get_rect()
						player_textRect.center = (1055, 200)
						my_surface.blit(player_text, player_textRect)

				winner = win(game_board, player)
				
				if winner[0] and ok == True:
					draw_board(my_surface, old_game_board, game_board, (turn % 2) + 1, cell_size, turn, True)
					win_text1 = font.render("Player " + str(winner[1]), True, get_player_color(winner[1]))
					win_text2 = font.render("won!", True, get_player_color(winner[1]))
					win_textRect1 = win_text1.get_rect()
					win_textRect2 = win_text2.get_rect()
					win_textRect1.center = (1055, 170)
					win_textRect2.center = (1055, 230)
					my_surface.blit(win_text1, win_textRect1)
					my_surface.blit(win_text2, win_textRect2)
					in_progress = False

				player = (turn % 2) + 1

			elif event.type == QUIT:
				pygame.quit()
				return -1

		pygame.display.update()

	leave = drawButton(my_surface, "Quit", 900, 500, 298, 52)
	replay = drawButton(my_surface, "Play again", 900, 600, 298, 52)
	pygame.display.update()

	while True:
		for event in pygame.event.get():
			if event.type == QUIT:
				pygame.quit()
				return -1
			if event.type == MOUSEBUTTONDOWN:
				x,y = event.pos
				if x >= leave.left and x <= leave.right and y >= leave.top and y <= leave.bottom:
					return -1
				elif x >= replay.left and x <= replay.right and y >= replay.top and y <= replay.bottom:
					return 1
