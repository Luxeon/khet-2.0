def initGameboard(n):

	gameboard = [ [ [0, 0, 0] for i in range(10) ] for j in range(8) ]

	# Format du plateau
	# gameboard[ligne][colonne][player/pièce/orientation]
	# Player: 1(rouge) ou 2(blanc)
	# Pièces:
	# 1 = Pharaon
	# 2 = Scarabée
	# 3 = Anubis
	# 4 = Pyramide
	# 5 = Sphinx
	# Orientation:
	# 1 = Nord
	# 2 = Est
	# 3 = Sud
	# 4 = Ouest


	if n == 1:
		print("Plateau sélectionné: Classic")

		boardfile = open("assets/init_conf/classic.txt","r")
	elif n == 2:
		print("Plateau sélectionné: Imhotep")

		boardfile = open("assets/init_conf/imhotep.txt","r")
	elif n == 3:
		print("Plateau sélectionné: Dynasty")

		boardfile = open("assets/init_conf/dynasty.txt","r")

	for line in boardfile:
		gameboard[int(line[0])][int(line[1])] = [int(line[2]), int(line[3]), int(line[4])]

	boardfile.close()

	return gameboard

def displaygameboard(gameboard):
	for line in gameboard:
		for elm in line:

			if elm[0] == 0:
				print("_",end="")
			else:
				print(elm[0],end="")

			if elm[1] == 0:
				print("___|",end="")
			elif elm[1] == 1:
				print("H",end="")
			elif elm[1] == 2:
				print("S",end="")
			elif elm[1] == 3:
				print("A",end="")
			elif elm[1] == 4:
				print("P",end="")
			elif elm[1] == 5:
				print("X",end="")

			if elm[1] == 2 or elm[1] == 4:
				if elm[2] == 1:
					print("NE",end="|")
				elif elm[2] == 2:
					print("SE",end="|")
				elif elm[2] == 3:
					print("SO",end="|")
				elif elm[2] == 4:
					print("NO",end="|")
			else:
				if elm[2] == 1:
					print("N_",end="|")
				elif elm[2] == 2:
					print("E_",end="|")
				elif elm[2] == 3:
					print("S_",end="|")
				elif elm[2] == 4:
					print("O_",end="|")
		print("")

def select(gameboard,player):

	print("Player ",player,":")

	i = int(input("Ligne de la pièce à bouger: "))
	j = int(input("Colonne de la pièce à bouger: "))

	while possible(gameboard,player,i,j) == False:
		print("Erreur: Coordonnées non valides")
		i = int(input("Ligne de la pièce à bouger: "))
		j = int(input("Colonne de la pièce à bouger: "))

	return i,j

def possible(gameboard,player,i,j):

	if i >= 0 and i <= 7 and j >= 0 and j <= 9:
		if gameboard[i][j][0] == player:
			return True
		else:
			return False
	else:
		return False

def actions(gameboard,i,j):

	if gameboard[i][j][1] == 1:
		return 2
	elif gameboard[i][j][1] == 5:
		return 1
	else:
		for n in range(i-1,i+2):
			if n < 0 or n > 7:
				continue
			for m in range(j-1,j+2):
				if m < 0 or m > 9:
					continue
				if n != i or m != j:
					if gameboard[n][m][0] == 0:
						x = int(input("Que faire ?\n\t1) Rotation de 90°\n\t2) Déplacement\nVotre choix: "))

						while x != 1 and x != 2:
							print("Erreur: Choix invalide")
							x = int(input("Que faire ?\n\t1) Rotation de 90°\n\t2) Déplacement\nVotre choix: "))

						return x

def rotation(gameboard,i,j):

	if gameboard[i][j][1] == 5 or gameboard[i][j][1] == 2:
		if gameboard[i][j][2] == 1:
			gameboard[i][j][2] = 4
		elif gameboard[i][j][2] == 2:
			gameboard[i][j][2] = 3
		elif gameboard[i][j][2] == 3:
			gameboard[i][j][2] = 2
		elif gameboard[i][j][2] == 4:
			gameboard[i][j][2] = 1
	else:
		n = int(input("Dans quel sens souhaitez-vous tourner la pièce\n\t1) Sens horaire\n\t2) Sens anti-horaire\nVotre choix: "))

		while n != 1 and n != 2:
			print("Erreur: Choix invalide")
			n = int(input("Dans quel sens souhaitez-vous tourner la pièce\n\t1) Sens horaire\n\t2) Sens anti-horaire\nVotre choix: "))

		if n == 1:
			if gameboard[i][j][2] != 4:
				gameboard[i][j][2] += 1
			else:
				gameboard[i][j][2] = 1
		elif n == 2:
			if gameboard[i][j][2] != 1:
				gameboard[i][j][2] -= 1
			else:
				gameboard[i][j][2] = 4

def deplacement(gameboard,i,j,player):

	possible = False

	while possible == False:
		n = int(input("Dans quelle direction déplacer la pièce:\n\t8  1  2\n\t \ | / \n\t7-   -3\n\t / | \ \n\t6  5  4\nVotre choix: "))

		while n < 1 and n > 8:
			print("Erreur: Choix invalide")
			n = int(input("Dans quelle direction déplacer la pièce:\n\t8  1  2\n\t \ | / \n\t7-   -3\n\t / | \ \n\t6  5  4\nVotre choix: "))

		if n == 1 or n == 2 or n == 8:
			x = i - 1
		elif n == 4 or n == 5 or n == 6:
			x = i + 1
		else:
			x = i

		if n == 6 or n == 7 or n == 8:
			y = j - 1
		elif n == 2 or n == 3 or n == 4:
			y = j + 1
		else:
			y = j

		if x < 0 or x > 7 or y < 0 or y > 9:
			print("Impossible de déplacer la pièce: Case non valide")
		elif (player == 1 and (y == 9 or (y == 1 and (x == 0 or x == 7)))) or (player == 2 and (y == 0 or (y == 8 and (x == 0 or x == 7)))):
			print("Impossible de déplacer la pièce: Case réservée")
		elif gameboard[x][y][0] != 0:
			print("Impossible de déplacer la pièce: Case déjà occupée")
		else:
			gameboard[x][y] = gameboard[i][j]
			gameboard[i][j] = [0, 0, 0]
			possible = True

def laser(gameboard,player):

	if player == 1:
		i = 0
		j = 0
	elif player == 2:
		i = 7
		j = 9

	if gameboard[i][j][2] == 1:
		d = [0, -1]
	elif gameboard[i][j][2] == 2:
		d = [1, 0]
	elif gameboard[i][j][2] == 3:
		d = [0, 1]
	elif gameboard[i][j][2] == 4:
		d = [-1, 0]

	end = False

	while end == False:

		i += d[1]
		j += d[0]

		if i < 0 or i > 7 or j < 0 or j > 9:
					break

		while gameboard[i][j][0] == 0:

			i += d[1]
			j += d[0]
			if i < 0 or i > 7 or j < 0 or j > 9:
				break

		end = collision(d,gameboard,i,j)

def collision(d,gameboard,i,j):

	if i < 0 or i > 7 or j < 0 or j > 9:
		return True

	elif gameboard[i][j][1] == 1:
		gameboard[i][j] = [0, 0, 0]

		return True

	elif gameboard[i][j][1] == 2:
		if gameboard[i][j][2] == 1 or gameboard[i][j][2] == 3:
			if d[0] == 0:
				d[0] = d[1]
				d[1] = 0
			else:
				d[1] = d[0]
				d[0] = 0
		elif gameboard[i][j][2] == 2 or gameboard[i][j][2] == 4:
			if d[0] == 0:
				d[0] = - d[1]
				d[1] = 0
			else:
				d[1] = - d[0]
				d[0] = 0

		return False

	elif gameboard[i][j][1] == 3:
		if (d == [0, 1] and gameboard[i][j][2] != 1) or (d == [-1, 0] and gameboard[i][j][2] != 2) or (d == [0, -1] and gameboard[i][j][2] != 3) or (d == [1, 0] and gameboard[i][j][2] != 4):
			gameboard[i][j] = [0, 0, 0]

		return True

	elif gameboard[i][j][1] == 4:
		if ((d == [1, 0] or d == [0, -1]) and gameboard[i][j][2] == 1) or ((d == [1, 0] or d == [0, 1]) and gameboard[i][j][2] == 2) or ((d == [-1, 0] or d == [0, 1]) and gameboard[i][j][2] == 3) or ((d == [-1, 0] or d == [0, -1]) and gameboard[i][j][2] == 4):
			gameboard[i][j] = [0, 0, 0]

			return True
		else:
			if gameboard[i][j][2] == 1 or gameboard[i][j][2] == 3:
				if d[0] == 0:
					d[0] = d[1]
					d[1] = 0
				else:
					d[1] = d[0]
					d[0] = 0
			elif gameboard[i][j][2] == 2 or gameboard[i][j][2] == 4:
				if d[0] == 0:
					d[0] = - d[1]
					d[1] = 0
				else:
					d[1] = - d[0]
					d[0] = 0

			return False

	elif gameboard[i][j][1] == 5:
		return True

def win(gameboard,player):

	if player == 1:
		adv = 2
	else:
		adv = 1

	player_pharaoh = False
	adv_pharaoh = False

	for i in range(8):
		for j in range(10):
			if gameboard[i][j][0] == player and gameboard[i][j][1] == 1:
				player_pharaoh = True
			elif gameboard[i][j][0] == adv and gameboard[i][j][1] == 1:
				adv_pharaoh = True

	if player_pharaoh == False:
		print("Joueur ",adv," a gagné !")
		return True, adv
	elif adv_pharaoh == False:
		print("Joueur ",player," a gagné !")
		return True, player
	else:
		return False, -1

