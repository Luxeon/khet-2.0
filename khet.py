from board_interface import *
from configurator import *

import pygame, sys, os
from pygame.locals import *

pygame.init()

size = pygame.display.Info()
os.environ['SDL_VIDEO_WINDOW_POS'] = str((size.current_w - 1280)//2) + "," + str((size.current_h - 720)//2)

WIDTH = 1280
HEIGHT = 720

WHITE = (255, 255, 255)

pygame.mixer.music.load("assets/music.mp3")
pygame.mixer.music.play(-1)

def drawButton(maSurface, monTexte, pos_x, pos_y, end_x, end_y):

    myRect = pygame.Rect(pos_x, pos_y, end_x, end_y)

    button = pygame.image.load('assets/sprites/button.png')
    button = pygame.transform.smoothscale(button, (end_x, end_y))
    maSurface.blit(button, (pos_x, pos_y))

    font = pygame.font.Font("assets/LHF_Uncial_Caps.ttf", (end_y * 2)//4)

    content = font.render(monTexte, True, (242, 230, 218))
    contentRect = content.get_rect()
    contentRect.center = (pos_x + end_x / 2, pos_y + end_y / 2)

    maSurface.blit(content, contentRect)

    return myRect


def accueil(maSurface):

    background = pygame.image.load('assets/sprites/menu_background.png')
    background = pygame.transform.smoothscale(background, (WIDTH, HEIGHT))
    maSurface.blit(background,(0,0))

    new_game = drawButton(maSurface,"New Game",96,600,298,52)
    load_save = drawButton(maSurface,"Controls",491,600,298,52)
    rules = drawButton(maSurface,"Quit",886,600,298,52)

    inProgress = True

    while inProgress:
        for event in pygame.event.get():
            if event.type == QUIT:
                inProgress = False
                return -1
            if event.type == MOUSEBUTTONDOWN:
                x,y = event.pos
                if x >= new_game.left and x <= new_game.right and y >= new_game.top and y <= new_game.bottom:
                    return 1
                elif x >= load_save.left and x <= load_save.right and y >= load_save.top and y <= load_save.bottom:
                    return 2
                elif x >= rules.left and x <= rules.right and y >= rules.top and y <= rules.bottom:
                    return 3

        pygame.display.update()

def newGame(maSurface):

    background = pygame.image.load('assets/sprites/background.png')
    background = pygame.transform.smoothscale(background, (WIDTH, HEIGHT))
    maSurface.blit(background,(0,0))

    font50 = pygame.font.Font("assets/LHF_Uncial_Caps.ttf",50)
    
    title = font50.render("Choice of Game Board",True,WHITE)
    titleRect = title.get_rect()
    titleRect.center = (430,70)
    maSurface.blit(title,titleRect)

    classic = drawButton(maSurface,"Classic",850,60,298,52)
    imhotep = drawButton(maSurface,"Imhotep",850,160,298,52)
    dynasty = drawButton(maSurface,"Dynasty",850,260,298,52)
    perso = drawButton(maSurface,"Configure the board",850,360,298,52)
    retour = drawButton(maSurface,"Back",850,600,298,52)

    inProgress = True
    display = False

    while inProgress:
        
        x,y = pygame.mouse.get_pos()
        
        if x >= classic.left and x <= classic.right and y >= classic.top and y <= classic.bottom:
            if display == False:
                board = pygame.image.load('assets/sprites/classic_board.png')
                board = pygame.transform.smoothscale(board, (573, 462))
                maSurface.blit(board,(140,165))
                display = True
        elif x >= imhotep.left and x <= imhotep.right and y >= imhotep.top and y <= imhotep.bottom:
            if display == False:
                board = pygame.image.load('assets/sprites/imhotep_board.png')
                board = pygame.transform.smoothscale(board, (573, 462))
                maSurface.blit(board,(140,165))
                display = True
        elif x >= dynasty.left and x <= dynasty.right and y >= dynasty.top and y <= dynasty.bottom:
            if display == False:
                board = pygame.image.load('assets/sprites/dynasty_board.png')
                board = pygame.transform.smoothscale(board, (573, 462))
                maSurface.blit(board,(140,165))
                display = True
        elif x >= perso.left and x <= perso.right and y >= perso.top and y <= perso.bottom:
            if display == False:
                font70 = pygame.font.Font("assets/LHF_Uncial_Caps.ttf",50)
                perso_text = font70.render("Create your own board!",True,WHITE)
                perso_textRect = perso_text.get_rect()
                perso_textRect.center = (430,380)
                maSurface.blit(perso_text,perso_textRect)
                display = True
        else:
            if display == True:
                display = False
            else:
                maSurface.blit(background,(0,0))
                maSurface.blit(title,titleRect)
                drawButton(maSurface,"Classic",850,60,298,52)
                drawButton(maSurface,"Imhotep",850,160,298,52)
                drawButton(maSurface,"Dynasty",850,260,298,52)
                drawButton(maSurface,"Configure the board",850,360,298,52)
                drawButton(maSurface,"Back",850,600,298,52)
        

        for event in pygame.event.get():
            if event.type == QUIT:
                inProgress = False
                return False, -1
            if event.type == MOUSEBUTTONDOWN:
                x,y = event.pos
                if x >= classic.left and x <= classic.right and y >= classic.top and y <= classic.bottom:
                    return True, 1
                elif x >= imhotep.left and x <= imhotep.right and y >= imhotep.top and y <= imhotep.bottom:
                    return True, 2
                elif x >= dynasty.left and x <= dynasty.right and y >= dynasty.top and y <= dynasty.bottom:
                    return True, 3
                elif x >= perso.left and x <= perso.right and y >= perso.top and y <= perso.bottom:
                    return True, 4
                elif x >= retour.left and x <= retour.right and y >= retour.top and y <= retour.bottom:
                    return False, 0

        pygame.display.update()

def rules(maSurface):
    left_button = pygame.image.load('assets/sprites/left_arrow.png')
    left_button = pygame.transform.smoothscale(left_button, (60, 63))
    maSurface.blit(left_button,(40,310))
    left = pygame.Rect(40,310,60,63)
    
    rule = pygame.image.load('assets/sprites/rules/1.png')
    rule = pygame.transform.smoothscale(rule, (WIDTH, HEIGHT))
    maSurface.blit(rule,(0,0))
    
    right_button = pygame.image.load('assets/sprites/right_arrow.png')
    right_button = pygame.transform.smoothscale(right_button, (60, 63))
    maSurface.blit(right_button,(1180,310))
    right = pygame.Rect(1180,310,60,63)
    
    retour = drawButton(maSurface,"Back",491,645,298,52)

    pygame.display.update()

    n = 1

    while True:
        for event in pygame.event.get():
            if event.type == QUIT:
                return -1
            if event.type == MOUSEBUTTONDOWN:
                x,y = event.pos
                if x >= left.left and x <= left.right and y >= left.top and y <= left.bottom and n > 1:
                    n -= 1
                elif x >= right.left and x <= right.right and y >= right.top and y <= right.bottom and n < 2:
                    n += 1
                elif x >= retour.left and x <= retour.right and y >= retour.top and y <= retour.bottom:
                    return False
                
                rule = pygame.image.load('assets/sprites/rules/' + str(n) + '.png')
                rule = pygame.transform.smoothscale(rule, (WIDTH, HEIGHT))
                maSurface.blit(rule,(0,0))

                if n > 1:
                    maSurface.blit(left_button,(40,310))
                if n < 2:
                    maSurface.blit(right_button,(1180,310))
                drawButton(maSurface,"Back",491,645,298,52)

                pygame.display.update()

def main_menu():
    
    maSurface = pygame.display.set_mode((WIDTH,HEIGHT))
    pygame.display.set_caption("Khet 2.0")
    icon = pygame.image.load("assets/icon.png")
    pygame.display.set_icon(icon)

    game_board = -1

    inProgress = True
    
    while inProgress:
        ok = False

        #choice = accueil(maSurface)
        
        while ok == False:
            choice = accueil(maSurface)

            if choice == -1:
                pygame.quit()
                return -1
            elif choice == 1:
                ok, board_type = newGame(maSurface)
                if board_type == -1:
                    pygame.quit()
                    return -1
                elif board_type == 4:
                    game_board = configurator()
                    if game_board == -1:
                        return -1
                    elif game_board == 1:
                        ok = False
            elif choice == 2:
                ok = rules(maSurface)
                if ok == -1:
                    pygame.quit()
                    return -1
                
            elif choice == 3:
                pygame.quit()
                return -1
                    
        font = pygame.font.Font("assets/LHF_Uncial_Caps.ttf",50)
        leave = main(game_board,board_type)

        if leave == 1:
            ok = True
            game_board = -1
        elif leave == -1:
            pygame.quit()
            return -1

    #pygame.quit()

main_menu()
