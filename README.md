Khet 2.0 is a virtual remake of the board game ***Khet: The Laser Game***.

This is a group project carried out in **Python** with the library Pygame during my 1st year of study. You can see a video presentation on my portfolio just right [here][1], in the Khet 2.0 section.

How to use it ?
---------------

* You can launch the game either with the .exe file or with the script `khet.py` with Python 3
* Once launched, you can start a game or check the controls
* If you start a game, you can choose between 3 game boards or you can create your own

Rules
-----

* You win the game if a laser hits the opponent's Pharaoh
* There are 5 types of pawns:
    * The **Pharaoh** exists in only one copy for each player. This is the piece to knock down in order to win the game. This one is eliminated when it is touched by a laser, without any other condition.
    * The **Sphinx** has only two possible orientations and cannot move. This is the pawn that will shoot a laser in the direction it is looking. Each player has one copy.
    * The **Scarab** (two-sides mirror) reflects lasers coming from any direction. There are 2 pawns per player. He can also move to an adjacent square occupied by a Pyramid or an Anubis, in which case they switch positions.
    * The **Pyramid** (one-side mirror) reflects the lasers coming from two directions, and is eliminated by the lasers touching its other sides. 7 Pyramids are available to each player.
    * The **Anubis** absorbs lasers on its front side, and is killed by them on the others. There are 2 pawns per player.
* On a player's turn, he or she can choose between moving a piece to an adjacent square or rotating a piece 90 degrees. As a result of this action, that player's Sphinx fires its laser. The turn ends whether or not a piece is eliminated
* Moreover, the white player can't place pawns on red squares and vice versa for the red player

Installation
------------

It is possible for you to test the application by yourself locally. For this, the source files are at your disposal.

About me
--------

I am a Computer Science Master student. You can find some of my other projects on my [Portfolio][1]. 

[1]: https://portfolio.cyrilpiejougeac.dev
