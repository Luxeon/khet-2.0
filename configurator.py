import pygame, sys
from pygame.locals import *

fpsClock = pygame.time.Clock()
FPS = 30

WHITE = (255, 255, 255)
BROWN = (147,92,51)
WIDTH = 1280
HEIGHT = 720


def drawButton(maSurface, monTexte, pos_x, pos_y, end_x, end_y):

    myRect = pygame.Rect(pos_x, pos_y, end_x, end_y)

    button = pygame.image.load('assets/sprites/button.png')
    button = pygame.transform.smoothscale(button, (end_x, end_y))
    maSurface.blit(button, (pos_x, pos_y))

    font = pygame.font.Font("assets/LHF_Uncial_Caps.ttf", (end_y * 2)//4)

    content = font.render(monTexte, True, (242, 230, 218))
    contentRect = content.get_rect()
    contentRect.center = (pos_x + end_x / 2, pos_y + end_y / 2)

    maSurface.blit(content, contentRect)

    return myRect


def drawBoard(maSurface, gameboard, pawns):

    background = pygame.image.load('assets/sprites/config_background.png')
    background = pygame.transform.smoothscale(background, (WIDTH, HEIGHT))
    maSurface.blit(background,(0,0))

    font = pygame.font.Font("assets/LHF_Uncial_Caps.ttf",50)

    pawn_text1 = font.render("Place",True,WHITE)
    pawn_textRect1 = pawn_text1.get_rect()
    pawn_textRect1.center = (1055,80)
    maSurface.blit(pawn_text1,pawn_textRect1)
    pawn_text2 = font.render("the pawns",True,WHITE)
    pawn_textRect2 = pawn_text2.get_rect()
    pawn_textRect2.center = (1055,130)
    maSurface.blit(pawn_text2,pawn_textRect2)
    
    global cell_size
    cell_size = 67
    global grid
    grid = pygame.Rect(0, 0, cell_size * 10, cell_size * 8)
    pygame.Rect.move_ip(grid, 105, 93)
    global pawngrid
    pawngrid = pygame.Rect(0, 0, cell_size * 2 + cell_size // 2, cell_size * 4 + 3 * cell_size // 2)
    pygame.Rect.move_ip(pawngrid, 973, 200)

    # Affichage de la grid

    for i in range(8):
        for j in range(10):
            pygame.draw.rect(maSurface, BROWN, (grid.x + j * cell_size, grid.y + i * cell_size, cell_size, cell_size), 3)


    # Affichage des pieces de la grid

    for i in range(8):
        for j in range(10):
            if gameboard[i][j][0] != 0:
                if gameboard[i][j][0] == 1:
                    if gameboard[i][j][1] == 1:
                        pawn = pygame.image.load("assets/sprites/" + str(gameboard[i][j][2]) + "/pharaon_red.png")
                    elif gameboard[i][j][1] == 2:
                        pawn = pygame.image.load("assets/sprites/" + str(gameboard[i][j][2]) + "/scarab_red.png")
                    elif gameboard[i][j][1] == 3:
                        pawn = pygame.image.load("assets/sprites/" + str(gameboard[i][j][2]) + "/anubis_red.png")
                    elif gameboard[i][j][1] == 4:
                        pawn = pygame.image.load("assets/sprites/" + str(gameboard[i][j][2]) + "/pyramid_red.png")
                    elif gameboard[i][j][1] == 5:
                        pawn = pygame.image.load("assets/sprites/" + str(gameboard[i][j][2]) + "/sphynx_red.png")
                elif gameboard[i][j][0] == 2:
                    if gameboard[i][j][1] == 1:
                        pawn = pygame.image.load("assets/sprites/" + str(gameboard[i][j][2]) + "/pharaon_white.png")
                    elif gameboard[i][j][1] == 2:
                        pawn = pygame.image.load("assets/sprites/" + str(gameboard[i][j][2]) + "/scarab_white.png")
                    elif gameboard[i][j][1] == 3:
                        pawn = pygame.image.load("assets/sprites/" + str(gameboard[i][j][2]) + "/anubis_white.png")
                    elif gameboard[i][j][1] == 4:
                        pawn = pygame.image.load("assets/sprites/" + str(gameboard[i][j][2]) + "/pyramid_white.png")
                    elif gameboard[i][j][1] == 5:
                        pawn = pygame.image.load("assets/sprites/" + str(gameboard[i][j][2]) + "/sphynx_white.png")

                pawn = pygame.transform.smoothscale(pawn, (cell_size, int(cell_size * 550 / 325)))
                pawnRect = pygame.Rect(j * cell_size + 105, i * cell_size + 80 - (int(cell_size * 550 / 325) - cell_size), cell_size, int(cell_size * 550 / 325))

                maSurface.blit(pawn, pawnRect)

                

    # Affichage des pieces de la pawngrid

    for i in range(2):
        for j in range(4):
            if pawns[i][j][2] != 0:
                if pawns[i][j][0] == 1:
                    if pawns[i][j][1] == 1:
                        pawn = pygame.image.load("assets/sprites/3/pharaon_red.png")
                    elif pawns[i][j][1] == 2:
                        pawn = pygame.image.load("assets/sprites/3/scarab_red.png")
                    elif pawns[i][j][1] == 3:
                        pawn = pygame.image.load("assets/sprites/3/anubis_red.png")
                    elif pawns[i][j][1] == 4:
                        pawn = pygame.image.load("assets/sprites/3/pyramid_red.png")
                elif pawns[i][j][0] == 2:
                    if pawns[i][j][1] == 1:
                        pawn = pygame.image.load("assets/sprites/3/pharaon_white.png")
                    elif pawns[i][j][1] == 2:
                        pawn = pygame.image.load("assets/sprites/3/scarab_white.png")
                    elif pawns[i][j][1] == 3:
                        pawn = pygame.image.load("assets/sprites/3/anubis_white.png")
                    elif pawns[i][j][1] == 4:
                        pawn = pygame.image.load("assets/sprites/3/pyramid_white.png")

                pawn = pygame.transform.smoothscale(pawn, (cell_size, int(cell_size * 550 / 325)))

                maSurface.blit(pawn, (pawngrid.x + i * (cell_size + cell_size // 2), pawngrid.y + j * (cell_size + cell_size // 2) - int(cell_size * 550 / 325) + cell_size))

    


def click(gameboard, pawns, x, y):
                    
    if pygame.Rect.collidepoint(pawngrid, x, y):
        for i in range(2):
            for j in range(4):
                if x > pawngrid.x + i * (cell_size + cell_size // 2) and x < pawngrid.x + i * (cell_size + cell_size // 2) + cell_size and y > pawngrid.y + j * (cell_size + cell_size // 2) and y < pawngrid.y + j * (cell_size + cell_size // 2) + cell_size:
                    if pawns[i][j][2] != 0:
                        if pawns[i][j][0] == 1:
                            if pawns[i][j][1] == 1:
                                pawn = pygame.image.load("assets/sprites/3/pharaon_red.png")
                            elif pawns[i][j][1] == 2:
                                pawn = pygame.image.load("assets/sprites/3/scarab_red.png")
                            elif pawns[i][j][1] == 3:
                                pawn = pygame.image.load("assets/sprites/3/anubis_red.png")
                            elif pawns[i][j][1] == 4:
                                pawn = pygame.image.load("assets/sprites/3/pyramid_red.png")
                        elif pawns[i][j][0] == 2:
                            if pawns[i][j][1] == 1:
                                pawn = pygame.image.load("assets/sprites/3/pharaon_white.png")
                            elif pawns[i][j][1] == 2:
                                pawn = pygame.image.load("assets/sprites/3/scarab_white.png")
                            elif pawns[i][j][1] == 3:
                                pawn = pygame.image.load("assets/sprites/3/anubis_white.png")
                            elif pawns[i][j][1] == 4:
                                pawn = pygame.image.load("assets/sprites/3/pyramid_white.png")

                        pawn = pygame.transform.smoothscale(pawn, (cell_size, int(cell_size * 550 / 325)))

                        if pawns[i][j][0] == 1:
                            data = [pawns[i][j][0], pawns[i][j][1], 3]
                        elif pawns[i][j][0] == 2:
                            data = [pawns[i][j][0], pawns[i][j][1], 1]

                        pawns[i][j][2] -= 1

                        return pawn, data, 1
                    
    elif pygame.Rect.collidepoint(grid, x, y):
        for i in range(8):
            for j in range(10):
                if x > grid.x + j * cell_size and x < grid.x + (j+1) * cell_size and y > grid.y + i * cell_size and y < grid.y + (i+1) * cell_size:
                    data = gameboard[i][j]

                    if gameboard[i][j][0] != 0 and gameboard[i][j][1] != 5:
                        if gameboard[i][j][0] == 1:
                            if gameboard[i][j][1] == 1:
                                pawn = pygame.image.load("assets/sprites/" + str(gameboard[i][j][2]) + "/pharaon_red.png")
                            elif gameboard[i][j][1] == 2:
                                pawn = pygame.image.load("assets/sprites/" + str(gameboard[i][j][2]) + "/scarab_red.png")
                            elif gameboard[i][j][1] == 3:
                                pawn = pygame.image.load("assets/sprites/" + str(gameboard[i][j][2]) + "/anubis_red.png")
                            elif gameboard[i][j][1] == 4:
                                pawn = pygame.image.load("assets/sprites/" + str(gameboard[i][j][2]) + "/pyramid_red.png")
                            elif gameboard[i][j][1] == 5:
                                pawn = pygame.image.load("assets/sprites/" + str(gameboard[i][j][2]) + "/sphynx_red.png")
                        elif gameboard[i][j][0] == 2:
                            if gameboard[i][j][1] == 1:
                                pawn = pygame.image.load("assets/sprites/" + str(gameboard[i][j][2]) + "/pharaon_white.png")
                            elif gameboard[i][j][1] == 2:
                                pawn = pygame.image.load("assets/sprites/" + str(gameboard[i][j][2]) + "/scarab_white.png")
                            elif gameboard[i][j][1] == 3:
                                pawn = pygame.image.load("assets/sprites/" + str(gameboard[i][j][2]) + "/anubis_white.png")
                            elif gameboard[i][j][1] == 4:
                                pawn = pygame.image.load("assets/sprites/" + str(gameboard[i][j][2]) + "/pyramid_white.png")
                            elif gameboard[i][j][1] == 5:
                                pawn = pygame.image.load("assets/sprites/" + str(gameboard[i][j][2]) + "/sphynx_white.png")

                        pawn = pygame.transform.smoothscale(pawn, (cell_size, int(cell_size * 550 / 325)))

                        gameboard[i][j] = [0, 0, 0]

                        return pawn, data, [i,j]
                    
    return [-1]

def unclick(gameboard, pawns, x, y, data, prv_pos):

    if pygame.Rect.collidepoint(grid, x, y):
        for i in range(8):
            for j in range(10):
                if x > grid.x + j * cell_size and x < grid.x + (j+1) * cell_size and y > grid.y + i * cell_size and y < grid.y + (i+1) * cell_size:
                    if gameboard[i][j][0] == 0 and ((data[0] == 1 and (j != 9 and (j != 1 or (i != 0 and i != 7)))) or (data[0] == 2 and (j != 0 and (j != 8 or (i != 0 and i != 7))))):
                        gameboard[i][j] = data
                    else:
                        if prv_pos == 1:
                            pawns[data[0]-1][data[1]-1][2] += 1
                        else:
                            gameboard[prv_pos[0]][prv_pos[1]] = data
    else:
        if prv_pos == 1:
            pawns[data[0]-1][data[1]-1][2] += 1
        else:
            gameboard[prv_pos[0]][prv_pos[1]] = data

def rotate(gameboard,x,y):
    
    if pygame.Rect.collidepoint(grid, x, y):
        for i in range(8):
            for j in range(10):
                if x > grid.x + j * cell_size and x < grid.x + (j+1) * cell_size and y > grid.y + i * cell_size and y < grid.y + (i+1) * cell_size:
                    if gameboard[i][j][2] == 4:
                        gameboard[i][j][2] = 1
                    elif gameboard[i][j][1] == 5:
                        if gameboard[i][j][0] == 1:
                            if gameboard[i][j][2] == 2:
                                gameboard[i][j][2] = 3
                            else:
                                gameboard[i][j][2] = 2
                        elif gameboard[i][j][0] == 2:
                            if gameboard[i][j][2] == 1:
                                gameboard[i][j][2] = 4
                            else:
                                gameboard[i][j][2] = 1
                    else:
                        gameboard[i][j][2] += 1

def nbr_pawn(pawns):

    for i in range(2):
        for j in range(4):
            if pawns[i][j][2] != 0:
                return False
    return True


def configurator():

    maSurface = pygame.display.set_mode((WIDTH, HEIGHT))
    pygame.display.set_caption("Khet 2.0")

    gameboard = [ [ [0, 0, 0] for i in range(10) ] for j in range(8) ]
    gameboard[0][0] = [1, 5, 3]
    gameboard[7][9] = [2, 5, 1]
    pawns = [[[1, 1, 1], [1, 2, 2], [1, 3, 2], [1, 4, 7]], [[2, 1, 1], [2, 2, 2], [2, 3, 2], [2, 4, 7]]]

    inProgress = True
    end = False

    drawBoard(maSurface, gameboard, pawns)
    valider = drawButton(maSurface,"Validate",890,640,150,52)
    annuler = drawButton(maSurface,"Cancel",1080,640,150,52)

    while inProgress:
        for event in pygame.event.get():
            if event.type == QUIT:
                inProgress = False
                pygame.quit()
                return -1
            if event.type == MOUSEBUTTONDOWN and event.button == 1:
                x, y = event.pos
                
                if x >= valider.left and x <= valider.right and y >= valider.top and y <= valider.bottom and end == True:
                    #pygame.quit()
                    return gameboard
                elif x >= annuler.left and x <= annuler.right and y >= annuler.top and y <= annuler.bottom:
                    return 1
                
                pawn = click(gameboard, pawns, x, y)
                drawBoard(maSurface, gameboard, pawns)
                drawButton(maSurface,"Validate",890,640,150,52)
                drawButton(maSurface,"Cancel",1080,640,150,52)
                pygame.image.save(maSurface, "assets/sprites/screen.png")
                if pawn[0] != -1:
                    inProg = True
                    while inProg:
                        x, y = event.pos
                        screen = pygame.image.load('assets/sprites/screen.png')
                        maSurface.blit(screen, (0,0))
                        maSurface.blit(pawn[0], (x - cell_size / 2, y - cell_size / 2 - int(cell_size * 550 / 325) + cell_size))
                        for event in pygame.event.get():
                            if event.type == MOUSEBUTTONUP:
                                unclick(gameboard, pawns, x, y, pawn[1], pawn[2])
                                drawBoard(maSurface, gameboard, pawns)
                                drawButton(maSurface,"Validate",890,640,150,52)
                                drawButton(maSurface,"Cancel",1080,640,150,52)
                                inProg = False
                        fpsClock.tick(FPS)
                        pygame.display.update()
                    end = nbr_pawn(pawns)
            if event.type == MOUSEBUTTONDOWN and event.button == 3:
                x, y = event.pos
                rotate(gameboard,x,y)
                drawBoard(maSurface, gameboard, pawns)
                drawButton(maSurface,"Validate",890,640,150,52)
                drawButton(maSurface,"Cancel",1080,640,150,52)
                
        pygame.display.update()

    


